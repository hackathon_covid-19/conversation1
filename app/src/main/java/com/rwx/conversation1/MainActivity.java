package com.rwx.conversation1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.vision.barcode.Barcode;
import com.notbytes.barcode_reader.BarcodeReaderActivity;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {


    final static int BARCODE_READER_ACTIVITY_REQUEST = 2137;

    String text_to_read;
    String entered_text;
    TextToSpeech tts;
    int conversation_status = 0;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    String room = null;
    String bed = null;
    int urgency = 0;
    int alarm_active = 0;
    int notice_sent_successfully = -1;
    boolean patient = true;
    boolean pain = false;
    int pain_intensity = 0;
    boolean breathing_problems = false;
    boolean fever = false;
    boolean cough = false;
    boolean diarrhea = false;
    String other_needs = "";
    boolean conversation_ended = false;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(patient)
        {
            sharedpreferences = getPreferences(MODE_PRIVATE);
            editor = sharedpreferences.edit();
            if (sharedpreferences.getBoolean("registered", false) == false) {
                Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
                startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
            } else {
                //Toast.makeText(this, "Użytkownik już zarejestrowany:" + sharedpreferences.getString("username", ">>error<<"), Toast.LENGTH_SHORT).show();

                tts = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {

                    @Override
                    public void onInit(int status) {
                        // TODO Auto-generated method stub
                        if (status == TextToSpeech.SUCCESS) {
                            tts.setSpeechRate(0.9f);
                            int result = tts.setLanguage(Locale.UK);//new Locale("pl_PL")
                            if (result == TextToSpeech.LANG_MISSING_DATA ||
                                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                Log.e("error", "This Language is not supported");
                            }
                        } else {
                            Log.e("error", "Initilization Failed!");
                        }
                        conversation();
                        //getSpeechInput();
                    }
                });
            }
        }
        else
        {
            Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
            startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
        }

    }

    public void getSpeechInput() {
        while(tts.isSpeaking())
        {

        }
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Doesn't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
            {
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //Toast.makeText(this, result.get(0), Toast.LENGTH_SHORT).show();
                    entered_text = result.get(0);
                    conversation();
                }
                break;
            }
            case BARCODE_READER_ACTIVITY_REQUEST:
            {
                if(patient)
                {
                    if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
                        Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
                        //Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
                        editor.putBoolean("registered", true);
                        String information[] = barcode.rawValue.split(";");
                        room = information[0];
                        bed = information[1];
                        editor.putString("room", room);
                        editor.putString("bed", bed);
                        editor.commit();
                        Toast.makeText(this, "Zarejestrowano w systemie.", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
                        Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
                        Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
                        String information[] = barcode.rawValue.split(";");
                        room = information[0];
                        bed = information[1];
                        clearNotice();
                    }
                }
                break;
            }
            default:
            {
                Toast.makeText(this, "Error occurred when scanning.", Toast.LENGTH_SHORT).show();
                break;
            }
        }

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub

        if(tts != null){

            tts.stop();
            tts.shutdown();
        }
        super.onStop();
    }

    public void onClick(View v){

        convertTextToSpeech();

    }

    private void exitAfterTimeout(int timeout)
    {
        try
        {
            Thread.sleep(timeout);
        }
        catch (Exception exception)
        {

        }
        System.exit(0);
    }

    private void sendNotice()
    {
        room = sharedpreferences.getString("room", null);
        bed = sharedpreferences.getString("bed", null);
        RequestQueue queue = Volley.newRequestQueue(this);
        String symptoms = "";
        boolean next_symptom = false;
        if(pain)
        {
            symptoms = "pain: ".concat(Integer.toString(pain_intensity)).concat("/10");
            next_symptom = true;
        }
        if(breathing_problems)
        {
            if(next_symptom)
            {
                symptoms = symptoms.concat(", ");
            }
            symptoms = symptoms.concat("breathing difficulties");
            next_symptom = true;
        }
        if(cough)
        {
            if(next_symptom)
            {
                symptoms = symptoms.concat(", ");
            }
            symptoms = symptoms.concat("cough");
            next_symptom = true;
        }
        if(fever)
        {
            if(next_symptom)
            {
                symptoms = symptoms.concat(", ");
            }
            symptoms = symptoms.concat("fever");
            next_symptom = true;
        }
        if(diarrhea)
        {
            if(next_symptom)
            {
                symptoms = symptoms.concat(", ");
            }
            symptoms = symptoms.concat("diarrhea");
        }
        String url ="https://www.guardianwat.ch/covid-19/submit_data_test.php?room=" + room + "&bed=" + bed + "&urgency=" + urgency + "&symptoms=" + symptoms + "&other_needs=" + other_needs;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast toast = Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG);
                        toast.show();
                        if(response.equals("OK"))
                        {
                            notice_sent_successfully = 1;
                            text_to_read = getString(R.string.sent);
                            convertTextToSpeech();
                            System.exit(0);
                        }
                        else
                        {
                            notice_sent_successfully = 0;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(MainActivity.this, "That didn't work!", Toast.LENGTH_LONG);
                toast.show();
            }
        });
        queue.add(stringRequest);
    }

    private void clearNotice()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://www.guardianwat.ch/covid-19/clear_notice.php?room=" + room + "&bed=" + bed;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast toast = Toast.makeText(MainActivity.this, response, Toast.LENGTH_LONG);
                        toast.show();
                        if(response.equals("OK"))
                        {
                            notice_sent_successfully = 1;
                            exitAfterTimeout(3000);
                        }
                        else
                        {
                            notice_sent_successfully = 0;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(MainActivity.this, "That didn't work!", Toast.LENGTH_LONG);
                toast.show();
            }
        });
        queue.add(stringRequest);
    }

    private void endConversation()
    {
        conversation_ended = true;
        sendNotice();
    }

    private void conversation()
    {
        boolean repeat_answer = false;
        switch(conversation_status)
        {
            case 0:
            {
                text_to_read = getString(R.string.choose_priority);
                conversation_status++;
                break;
            }
            case 1:
            {
                if(entered_text.toLowerCase().contains("critical") || entered_text.toLowerCase().contains("now") || entered_text.toLowerCase().contains("immediate") || entered_text.toLowerCase().contains("very"))
                {
                    alarm_active = 1;
                    urgency = 3;
                    text_to_read = getString(R.string.dispatch);
                    endConversation();
                }
                else if(entered_text.toLowerCase().contains("not") || entered_text.toLowerCase().contains("wait"))
                {
                    alarm_active = 1;
                    urgency = 1;
                    text_to_read = getString(R.string.category);
                    conversation_status++;
                }
                else if(entered_text.toLowerCase().contains("urgent"))
                {
                    alarm_active = 1;
                    urgency = 2;
                    text_to_read = getString(R.string.category);
                    conversation_status++;
                }
                else
                {
                    text_to_read = getString(R.string.did_not_understand);
                    repeat_answer = true;
                }
                break;
            }
            case 2:
            {
                if(entered_text.toLowerCase().contains("ye") || entered_text.toLowerCase().contains("exactly") || entered_text.toLowerCase().contains("immediate"))
                {
                    urgency = 2;
                    conversation_status = 3;
                    text_to_read = getString(R.string.symptoms);
                }
                else if(entered_text.toLowerCase().contains("no"))
                {
                    conversation_status = 5;
                    text_to_read = getString(R.string.other_needs);
                }
                else
                {
                    text_to_read = getString(R.string.did_not_understand);
                    repeat_answer = true;
                }
                break;
            }
            case 3:
            {
                boolean wrong_answer = true;
                if(entered_text.toLowerCase().contains("breath"))
                {
                    breathing_problems = true;
                    text_to_read = getString(R.string.dispatch);
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("fever"))
                {
                    fever = true;
                    text_to_read = getString(R.string.dispatch);
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("cough"))
                {
                    cough = true;
                    text_to_read = getString(R.string.dispatch);
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("diarrhea"))
                {
                    diarrhea = true;
                    text_to_read = getString(R.string.dispatch);
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("pain"))
                {
                    pain = true;
                    conversation_status = 4;
                    text_to_read = getString(R.string.pain_intensity);
                    wrong_answer = false;
                }
                if(wrong_answer)
                {
                    text_to_read = getString(R.string.did_not_understand);
                    repeat_answer = true;
                }
                if(!pain && !wrong_answer)
                {
                    endConversation();
                }
                break;
            }
            case 4:
            {
                boolean wrong_answer = true;
                if(entered_text.toLowerCase().contains("one") || entered_text.contains("1"))
                {
                    pain_intensity = 1;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("two") || entered_text.contains("2"))
                {
                    pain_intensity = 2;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("three") || entered_text.contains("3"))
                {
                    pain_intensity = 3;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("four") || entered_text.contains("4"))
                {
                    pain_intensity = 4;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("five") || entered_text.contains("5"))
                {
                    pain_intensity = 5;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("six") || entered_text.contains("6"))
                {
                    pain_intensity = 6;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("seven") || entered_text.contains("7"))
                {
                    pain_intensity = 7;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("eight") || entered_text.contains("8"))
                {
                    pain_intensity = 8;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("nine") || entered_text.contains("9"))
                {
                    pain_intensity = 9;
                    wrong_answer = false;
                }
                if(entered_text.toLowerCase().contains("ten") || entered_text.contains("10"))
                {
                    pain_intensity = 10;
                    wrong_answer = false;
                }
                text_to_read = getString(R.string.dispatch);
                if(wrong_answer)
                {
                    text_to_read = getString(R.string.did_not_understand);
                    repeat_answer = true;
                }
                if(pain_intensity > 6)
                {
                    urgency = 3;
                }
                if(!repeat_answer)
                {
                    endConversation();
                }
                break;
            }
            case 5:
            {
                other_needs = entered_text.toLowerCase();
                text_to_read = getString(R.string.dispatch);
                endConversation();
                break;
            }
        }
        convertTextToSpeech();
        if(!conversation_ended)
        {
            getSpeechInput();
        }
    }

    private void convertTextToSpeech() {
        // TODO Auto-generated method stub
        if(text_to_read ==null||"".equals(text_to_read))
        {
            text_to_read = "Content not available";
            tts.speak(text_to_read, TextToSpeech.QUEUE_FLUSH, null);
        }else {
            tts.speak(text_to_read, TextToSpeech.QUEUE_FLUSH, null);
            while(tts.isSpeaking())
            {

            }
            try
            {
                Thread.sleep(500);
            }
            catch (Exception exception)
            {

            }
        }
    }
}
